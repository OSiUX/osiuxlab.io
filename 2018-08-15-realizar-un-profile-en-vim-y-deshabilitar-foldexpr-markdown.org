#+TITLE:       Realizar un /profile/ en =vim= y deshabilitar =Foldexpr_markdown=
#+AUTHOR:      Osiris Alejandro Gómez
#+EMAIL:       osiux@osiux.com
#+LANGUAGE:    es
#+LINK_HOME:   index.html
#+INCLUDE:     header.org
#+DATE:        2018-08-15 02:06
#+HTML_HEAD:   <meta property="og:title" content="Realizar un /profile/ en =vim= y deshabilitar =Foldexpr_markdown=" />
#+HTML_HEAD:   <meta property="og:type" content="article" />
#+HTML_HEAD:   <meta property="og:article:published_time" content="2018-08-15" />
#+HTML_HEAD:   <meta property="og:article:author" content="Osiris Alejandro Gómez" />
#+HTML_HEAD:   <meta property="og:url" content="https://osiux.com/2018-08-15-realizar-un-profile-en-vim-y-deshabilitar-foldexpr-markdown.html" />
#+HTML_HEAD:   <meta property="og:site_name" content="OSiUX" />
#+HTML_HEAD:   <meta property="og:locale" content="es_AR" />


Hace unos días que vengo notando que =vim= se cuelga o al menos tarda
demasiado al pegar texto.

Al principio pensaba que tenía que ver con =tmux=, por estar copiando y
pegando texto desde el /buffer/ de =tmux=.

Pero luego comprobé que al pegar en =mcedit= esto no pasaba, e
investigando cuál podría ser el problema descubrí dos cosas:

** Cómo realizar un /profile/ [fn:vim] en =vim= para detectar dónde esta la lentitud

Para iniciar el profile:

: :profile start profile.log
: :profile func *
: :profile file *
: " At this point do slow actions
: :profile pause
: :noautocmd qall!

Para visualizar rápidamente que fue lo más lento:

: # awk '{print $2,$3}' profile.log  | grep -v total | grep -v SORTED | sort -nr | head
:
: 27.629469 Foldexpr_markdown()
: 27.629469 Foldexpr_markdown()
: 1.671386 if
: 1.395355 if
: 1.186143 if
: 1.162478 if
: 1.044769 let
: 0.997596 let
: 0.834278 let
: 0.661260 if

** el plugin vim-markdown es lento

Encontré que el plugin /vim-markdown/ tiene un /bug/ [fn:bug] y se
puede deshabilitar la función =Foldexpr_markdown()= fácilmente
agregando al =.vimrc= la siguiente línea:

: let g:vim_markdown_folding_disabled=1

Luego volví a repetir el mismo pegado y en el nuevo /profile/ ya no
apareció la función =Foldexpr_markdown= y fue muy rápido:

: # awk '{print $2,$3}' markdown.log  | grep -v SORTED | grep -v total | sort -nr | head
:
: 0.035887 0.000680
: 0.035887 0.000680
: 0.035855 0.002342
: 0.035855 0.002342
: 0.035534 0.000327
: 0.035207 <SNR>94_MarkdownHighlightSources()
: 0.035207 <SNR>94_MarkdownHighlightSources()
: 0.032944 0.000331
: 0.031879 0.000077
: 0.031723 0.000280

[fn:vim] https://stackoverflow.com/questions/12213597/how-to-see-which-plugins-are-making-vim-slow
[fn:bug] https://github.com/plasticboy/vim-markdown/issues/162

** ChangeLog

  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/bf3a61526ad2a73cecb77a18995f1d63494e3664][=2022-11-13 20:39=]] agregar y actualizar tags OpenGraph
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/e46ec52748a7ecc60f09c3b95e363e92eaa0bebc][=2019-04-18 00:21=]] Agregar hora en header date
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/882b1cda084f7c18362c8414a78dd65c7b968a3f][=2019-04-09 03:01=]] Recuperar archivos en .md y convertirlos a .org
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/7baf690685f092fdd19f8a8aea2e92d49b4f6299][=2019-04-08 02:11=]] Agregar archivos sin commitear :S
