#+TITLE:       De Fondo Negro a Fondo Blanco
#+AUTHOR:      Osiris Alejandro Gomez
#+EMAIL:       osiux@osiux.com
#+LANGUAGE:    es
#+LINK_HOME:   index.html
#+INCLUDE:     header.org
#+DATE:        2012-02-14 22:19 
#+HTML_HEAD:   <meta property="og:title" content="De Fondo Negro a Fondo Blanco" />
#+HTML_HEAD:   <meta property="og:type" content="article" />
#+HTML_HEAD:   <meta property="og:article:published_time" content="2012-02-14" />
#+HTML_HEAD:   <meta property="og:article:author" content="Osiris Alejandro Gomez" />
#+HTML_HEAD:   <meta property="og:url" content="https://osiux.com/2012-02-14-de-fondo-negro-a-fondo-blanco.html" />
#+HTML_HEAD:   <meta property="og:site_name" content="OSiUX" />
#+HTML_HEAD:   <meta property="og:locale" content="es_AR" />
#+HTML_HEAD:   <meta property="og:image" content="https://osiux.com/img/de-fondo-negro-a-fondo-blanco.png" />


En [[http://gcoop.coop][gcoop]], solemos auto-capacitarnos al menos
una vez por mes, algún socio que tenga ganas de explicar algo que le
resulte interesante compartir puede dar una charlita.

#+ATTR_HTML: :width 640 :height 480 :title las cooperativas construyen un mundo mejor
[[file:img/de-fondo-negro-a-fondo-blanco.png][file:tmb/de-fondo-negro-a-fondo-blanco.png]]

Esta vez, tuve la suerte de mostrar un breve resumen sobre cómo usar
[[http://orgmode.org][org-mode]] que es parte de *Emacs* y que
básicamente permite hacer de todo, planificación de proyectos, manejo
de agenda y calendario, ejecutar código y capturar resultados,
realizar tablas y planillas de cálculo, administración de tareas,
tomar simples notas, hacer mapas mentales, y mucho más. Entre varias
características, es posible obtener elegantes documentos en PDF o
HTML.

Para muestra, nada mejor que ver el texto [fn:texto] con el que realicé los
slides [fn:slides] de la charla, un simple archivo de texto plano, que al
presionar =C-c C-e p= se convierte en un LaTeX Beamer:

Seguramente iré mejorando los slides más adelante, pero escribir esa
charlita me tomó sólo 3hs y 25minutos

#+BEGIN_EXAMPLE
  * de fondo negro a fondo blanco
    :LOGBOOK:
    CLOCK: [2012-02-13 lun 23:45]--[2012-02-14 mar 03:10] =>  3:25
    :END:
#+END_EXAMPLE

[fn:texto]  http://pub.osiux.com/charlas/de-fondo-negro-a-fondo-blanco/de-fondo-negro-a-fondo-blanco.org
[fn:slides] http://pub.osiux.com/charlas/de-fondo-negro-a-fondo-blanco/de-fondo-negro-a-fondo-blanco.pdf

** ChangeLog

  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/bf3a61526ad2a73cecb77a18995f1d63494e3664][=2022-11-13 20:39=]] agregar y actualizar tags OpenGraph
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/e46ec52748a7ecc60f09c3b95e363e92eaa0bebc][=2019-04-18 00:21=]] Agregar hora en header date
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/5c8643b83930c6269569c76602608bd33f93008b][=2019-04-18 00:01=]] Corregir identación header #+INCLUDE:
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/bbc3bbc728f2a3eeb4fe2e0a012ee5d8d613e3ef][=2015-07-03 04:31=]] @ 00:05 hs - elimino #+OPTIONS: de todos los archivos excepto header.org
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/a2abb3b333c91e0d3c15ea9e93a21589bd4d86f7][=2015-07-03 03:59=]] @ 01:00 hs - reemplazo :alt por :title y cambios menores
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/74165280ffad770d1f8b8acbfa7f22b95459b52a][=2014-04-22 11:35=]] @ 00:34 hs - Agrego timestamp:nil
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/0baa20f806c177591f312d276cd37ac99f057604][=2013-06-05 02:58=]] @ 02:30 hs - responsive max-width css + refactor
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/5ad3755a3df07cdfbdc75d56cae06db2fee4b5f2][=2013-04-24 08:04=]] @ 01:50 hs - migro a org 8.0
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/e4dc30e2a202e7ad44bddc761065e9aeb0a31bd3][=2013-03-21 02:27=]] @ 01:58 hs - agrego cambios según git.osiux.com
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/652199f438b8e3b7f52720e2dc19208c9bcd7651][=2012-12-15 22:31=]] @ 04:00 hs - convert old blog in rST to org
