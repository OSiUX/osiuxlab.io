#+TITLE:       Hacks/Hackers Buenos Aires 2013
#+AUTHOR:      Osiris Alejandro Gómez
#+EMAIL:       osiux@osiux.com
#+LANGUAGE:    es
#+LINK_HOME:   index.html
#+INCLUDE:     header.org
#+DATE:        2013-08-30 10:46
#+HTML_HEAD:   <meta property="og:title" content="Hacks/Hackers Buenos Aires 2013" />
#+HTML_HEAD:   <meta property="og:type" content="article" />
#+HTML_HEAD:   <meta property="og:article:published_time" content="2013-08-30" />
#+HTML_HEAD:   <meta property="og:article:author" content="Osiris Alejandro Gómez" />
#+HTML_HEAD:   <meta property="og:url" content="https://osiux.com/2013-09-30-hacks-hackers-buenos-aires-2013.html" />
#+HTML_HEAD:   <meta property="og:site_name" content="OSiUX" />
#+HTML_HEAD:   <meta property="og:locale" content="es_AR" />
#+HTML_HEAD:   <meta property="og:image" content="https://osiux.com/img/hhba-2013/hhba-2013-09-30-11.jpg" />


** MediaParty

   Del 29 al 31 de Agosto de 2013, en el KONEX se realizó la edición
   2013 del *HHBA* [fn:hhba] junto a periodistas, programadores, diseñadores,
   nerds y geeks :-P

** Taller de Antenas

   *BuenosAiresLibres* [fn:bal] dió un taller de armado de antenas,
   del cuál participé mostrando cómo armar una Omni de 4 elementos.

   #+ATTR_HTML: :width 640 :height 480 :title HHBA Taller de Antenas
   [[file:img/hhba-2013/hhba-2013-09-30-11.jpg][file:tmb/hhba-2013/hhba-2013-09-30-11.jpg]]

   #+ATTR_HTML: :width 640 :height 480 :title HHBA Taller de Antenas
   [[file:img/hhba-2013/hhba-2013-09-30-27.jpg][file:tmb/hhba-2013/hhba-2013-09-30-27.jpg]]

   #+ATTR_HTML: :width 640 :height 480 :title HHBA Taller de Antenas
   [[file:img/hhba-2013/hhba-2013-09-30-38.jpg][file:tmb/hhba-2013/hhba-2013-09-30-38.jpg]]

   #+ATTR_HTML: :width 640 :height 480 :title HHBA Taller de Antenas
   [[file:img/hhba-2013/hhba-2013-09-30-44.jpg][file:tmb/hhba-2013/hhba-2013-09-30-44.jpg]]

   #+ATTR_HTML: :width 640 :height 480 :title HHBA Taller de Antenas
   [[file:img/hhba-2013/hhba-2013-09-30-52.jpg][file:tmb/hhba-2013/hhba-2013-09-30-52.jpg]]

   #+ATTR_HTML: :width 640 :height 480 :title HHBA Taller de Antenas
   [[file:img/hhba-2013/hhba-2013-09-30-54.jpg][file:tmb/hhba-2013/hhba-2013-09-30-54.jpg]]

** HowTo Antena OmniDireccional
   
   [[file:img/antena-omni-bal.png][file:img/antena-omni-bal.png]]

[fn:hhba] http://www.mediaparty.info/
[fn:bal] http://wiki.buenosaireslibre.org

** ChangeLog

  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/bf3a61526ad2a73cecb77a18995f1d63494e3664][=2022-11-13 20:39=]] agregar y actualizar tags OpenGraph
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/e46ec52748a7ecc60f09c3b95e363e92eaa0bebc][=2019-04-18 00:21=]] Agregar hora en header date
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/5c8643b83930c6269569c76602608bd33f93008b][=2019-04-18 00:01=]] Corregir identación header #+INCLUDE:
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/bbc3bbc728f2a3eeb4fe2e0a012ee5d8d613e3ef][=2015-07-03 04:31=]] @ 00:05 hs - elimino #+OPTIONS: de todos los archivos excepto header.org
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/a2abb3b333c91e0d3c15ea9e93a21589bd4d86f7][=2015-07-03 03:59=]] @ 01:00 hs - reemplazo :alt por :title y cambios menores
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/74165280ffad770d1f8b8acbfa7f22b95459b52a][=2014-04-22 11:35=]] @ 00:34 hs - Agrego timestamp:nil
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/221408559025ef55043b55d1380a26145ada0d47][=2014-01-04 11:57=]] @ 00:25 hs - Agrego ultimos cambios del 2013
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/5149bb21c63fa8db33e0dc84dd3716f141f2e745][=2013-09-10 10:14=]] @ 00:54 hs - Agrego mini resumen HHBA 2013
