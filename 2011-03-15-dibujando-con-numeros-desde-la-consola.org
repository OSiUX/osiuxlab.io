#+TITLE:       Dibujando con números desde la consola
#+AUTHOR:      Osiris Alejandro Gomez
#+EMAIL:       osiux@osiux.com
#+LANGUAGE:    es
#+LINK_HOME:   index.html
#+INCLUDE:     header.org
#+DATE:        2011-13-15 22:19
#+HTML_HEAD:   <meta property="og:title" content="Dibujando con números desde la consola" />
#+HTML_HEAD:   <meta property="og:type" content="article" />
#+HTML_HEAD:   <meta property="og:article:published_time" content="2011-13-15" />
#+HTML_HEAD:   <meta property="og:article:author" content="Osiris Alejandro Gomez" />
#+HTML_HEAD:   <meta property="og:url" content="https://osiux.com/2011-03-15-dibujando-con-numeros-desde-la-consola.html" />
#+HTML_HEAD:   <meta property="og:site_name" content="OSiUX" />
#+HTML_HEAD:   <meta property="og:locale" content="es_AR" />


Me puse a generar las potencias de 2 y logré un simpático gráfico
usando una línea de bash:

    # for i in $(seq 0 64);do echo $i | awk '{printf "%d ",(2^$1)}';done | par 64jl

El resultado

#+BEGIN_EXAMPLE

   1  2  4   8  16  32  64   128  256  512  1024   2048  4096  8192
   16384  32768   65536  131072   262144  524288   1048576  2097152
   4194304   8388608    16777216   33554432    67108864   134217728
   268435456    536870912    1073741824    2147483648    4294967296
   8589934592  17179869184   34359738368  68719476736  137438953472
   274877906944     549755813888    1099511627776     2199023255552
   4398046511104   8796093022208    17592186044416   35184372088832
   70368744177664  140737488355328 281474976710656  562949953421312
   1125899906842624        2251799813685248        4503599627370496
   9007199254740992       18014398509481984       36028797018963968
   72057594037927936      144115188075855872     288230376151711744
   576460752303423488    1152921504606846976    2305843009213693952
   4611686018427387904   9223372036854775808   18446744073709551616

#+END_EXAMPLE

La magia la realiza el comando =par= que es un justificador de texto,
donde le indico un ancho máximo de 64 caracteres y mediante los
parametros =l= que todas las líneas terminen con el mismo ancho y con la
opción =j= defino justificación.


** ChangeLog

  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/bf3a61526ad2a73cecb77a18995f1d63494e3664][=2022-11-13 20:39=]] agregar y actualizar tags OpenGraph
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/e46ec52748a7ecc60f09c3b95e363e92eaa0bebc][=2019-04-18 00:21=]] Agregar hora en header date
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/5c8643b83930c6269569c76602608bd33f93008b][=2019-04-18 00:01=]] Corregir identación header #+INCLUDE:
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/bbc3bbc728f2a3eeb4fe2e0a012ee5d8d613e3ef][=2015-07-03 04:31=]] @ 00:05 hs - elimino #+OPTIONS: de todos los archivos excepto header.org
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/74165280ffad770d1f8b8acbfa7f22b95459b52a][=2014-04-22 11:35=]] @ 00:34 hs - Agrego timestamp:nil
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/5ad3755a3df07cdfbdc75d56cae06db2fee4b5f2][=2013-04-24 08:04=]] @ 01:50 hs - migro a org 8.0
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/652199f438b8e3b7f52720e2dc19208c9bcd7651][=2012-12-15 22:31=]] @ 04:00 hs - convert old blog in rST to org
