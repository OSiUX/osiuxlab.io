#+TITLE:       OSiUX =graphviz=
#+AUTHOR:      Osiris Alejandro Gómez
#+EMAIL:       osiux@osiux.com
#+LANGUAGE:    es
#+LINK_HOME:   index.html
#+INCLUDE:     header.org
#+DATE:        {{{modification-time(%Y-%m-%d %H:%M)}}}
#+HTML_HEAD:   <meta property="og:title" content="OSiUX =graphviz=" />
#+HTML_HEAD:   <meta property="og:type" content="article" />
#+HTML_HEAD:   <meta property="og:article:published_time" content="2022-11-13" />
#+HTML_HEAD:   <meta property="og:article:author" content="Osiris Alejandro Gómez" />
#+HTML_HEAD:   <meta property="og:url" content="https://osiux.com/dot.html" />
#+HTML_HEAD:   <meta property="og:site_name" content="OSiUX" />
#+HTML_HEAD:   <meta property="og:locale" content="es_AR" />



** graficando grafos

A =graphviz= [fn:graphviz] lo descubrí en el 2007 y se volvió
indispensable, me la paso generando archivos =.dot=

El lenguaje =dot= es muy, pero muy simple, hay varios motores =dot=,
=neato=, =twopi=, =fdp= y =circo= que permiten generar grafos muy
diferentes, algunos ejemplos básicos encontralos en
/=visualizando grafos usando graphviz=/ [fn:grafos]

** galería de archivos =.dot=

... continuará ...

** te puede gustar...

   - [[file:imagenes-magicas-imagemagick.org][Imágenes Mágicas usando =imagemagick=]]
   - [[file:visualizando-grafos-graphviz.org][Visualizando grafos mediante =graphviz=]]
   - [[file:graficar-desde-consola.org][Graficar desde consola]]

[fn:graphviz]  https://graphviz.org/
[fn:grafos]    https://osiux.com/visualizando-grafos-graphviz.html
