#+TITLE:       OSiUX git repositories
#+AUTHOR:      Osiris Alejandro Gómez
#+EMAIL:       osiux@osiux.com
#+LANGUAGE:    es
#+LINK_HOME:   index.html
#+INCLUDE:     header.org
#+DATE:        {{{modification-time(%Y-%m-%d %H:%M)}}}
#+HTML_HEAD:   <meta property="og:title" content="OSiUX git repositories" />
#+HTML_HEAD:   <meta property="og:type" content="article" />
#+HTML_HEAD:   <meta property="og:article:published_time" content="2022-11-13" />
#+HTML_HEAD:   <meta property="og:article:author" content="Osiris Alejandro Gómez" />
#+HTML_HEAD:   <meta property="og:url" content="https://osiux.com/git-head.html" />
#+HTML_HEAD:   <meta property="og:site_name" content="OSiUX" />
#+HTML_HEAD:   <meta property="og:locale" content="es_AR" />



** =git-repos2org=

En base a un archivo =.git-repos= se /=genera dinámicamente=/ [fn:git]
*esta página* con un listado de todos los repositorios de proyectos que
mas activamente participo.

El /script/ es =git-repos2org= [fn:git-repos2org] y es parte de mis
utilidades =git= del repositorio =git-bash-utils= [fn:git-bash-utils]

[fn:git]            https://osiux.com/2021-02-05-git-repos2org.html
[fn:git-repos2org]  https://gitlab.com/osiux/git-bash-utils/-/raw/master/git-repos2org
[fn:git-bash-utils] https://gitlab.com/osiux/git-bash-utils/
