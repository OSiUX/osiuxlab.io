#+TITLE:       preparando varias charlas para los próximos eventos
#+AUTHOR:      Osiris Alejandro Gomez
#+EMAIL:       osiux@osiux.com
#+LANGUAGE:    es
#+LINK_HOME:   index.html
#+INCLUDE:     header.org
#+DATE:        2012-08-27 22:19
#+HTML_HEAD:   <meta property="og:title" content="preparando varias charlas para los próximos eventos" />
#+HTML_HEAD:   <meta property="og:type" content="article" />
#+HTML_HEAD:   <meta property="og:article:published_time" content="2012-08-27" />
#+HTML_HEAD:   <meta property="og:article:author" content="Osiris Alejandro Gomez" />
#+HTML_HEAD:   <meta property="og:url" content="https://osiux.com/2012-08-27-proximas-charlas-y-eventos.html" />
#+HTML_HEAD:   <meta property="og:site_name" content="OSiUX" />
#+HTML_HEAD:   <meta property="og:locale" content="es_AR" />
#+HTML_HEAD:   <meta property="og:image" content="https://osiux.com/img/COOPySL-CNEISI.png" />


Tengo un poco abandonado mi sitio web, obviamente estoy con muy poco tiempo y
en parte se debe al hecho de que voy a estar participando de varios eventos y
en muchos de ellos dando alguna que otra charla, paso un resumen para invitar
a todo aquél que tenga ganas de acercarse!


** 30/AGO al 01/SEP Hacks/Hackers BA Media Party

http://mediaparty.hhba.info

*Antenas y redes libres, cómo evitar que censuren internet*

Por su estructura descentralizada y autónoma, las redes libres son una
alternativa de comunicación y pueden evitar la censura y los limites en las
libertades de los usuarios que hoy día presenta internet.


** 05/SEP al 07/SEP CNEISI 2012

http://cneisi2012.frlp.utn.edu.ar/index.php?v=conferencias

*Cooperativismo y Software Libre*

En esta charla intentaremos, hablar de como compartir los beneficios económicos
del Software Libre, mientras, compartimos el código. O mas simple: "¿Por que
compartimos solo el código y no compartimos el rédito económico que este nos
deja?"

[[file:img/COOPySL-CNEISI.png]]

- [[http://gcoop.coop/COOPySL-CNEISI.pdf][Descargar PDF]]


** 15/SEP Taller de Comunicaciones Seguras y Software Libre

http://www.vialibre.org.ar/2012/08/25/15-de-septiembre-taller-de-seguridad-en-comunicaciones/

El taller está dirigido especialmente a todos aquellos que necesitan preservar
la confidencialidad de sus comunicaciones por correo electrónico y mantener
protegidos sus historiales de navegación en la web así como sus archivos
almacenados en sus computadoras. Periodistas, militantes sociales y activistas
necesitan medidas de seguridad adicionales para preservar sus fuentes, sus
datos y actividades. Para esto, y ante la vulnerabilidad de todos los sistemas
informáticos, es fundamental que aprendan a manejar programas que les permitan
entablar comunicaciones seguras y navegar por la red de manera anónima sin
dejar registros que puedan perjudicar sus investigaciones y acciones de
campaña. Históricamente, los periodistas y activistas sociales han sido objeto
de monitoreo, vigilancia y control por parte de actores que se pueden ver
afectados por su trabajo, por lo que las medidas de seguridad en sus
comunicaciones son indispensables.

[[file:img/priv-y-anon.png]]

- [[http://pub.osiux.com/priv-y-anon.pdf][Descargar PDF]]


** 19/SEP al 21/SEP Ekoparty 

Todavía no tengo ni entradas, pero seguramente estaré hablando de
[[http://redeslibres.org][RedesLibres]] y concretamente de
[[http://buenosaireslibre.org][BuenosAiresLibre]] junto al resto de
integrantes del proyecto ni bien tenga confirmada mi asistencia
(léase...mangazo!).


** 29/SEP Charlas Técnicas CaFe.IN

La continuación de las magistrales charlas ténicas trimestrales de
[[http://cafelug.org.ar][CaFeLUG]] ahora resucitaron en *CaFe.IN*

http://in.cafelug.org.ar/2012/08/2-programa-de-charlas---2012/index.html

*¿Que hice ayer? ¿Que tengo que hacer hoy?*

Aplicación práctica de diversas metodologías de planificación y organización de
proyectos usando [[http://orgmode.org][Org-mode]] para la gestión de tareas,
integración con BugTrackers, control de versiones y bases de datos. Registro de
tiempos, documentación y automatización de tests.

** ChangeLog

  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/bf3a61526ad2a73cecb77a18995f1d63494e3664][=2022-11-13 20:39=]] agregar y actualizar tags OpenGraph
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/e46ec52748a7ecc60f09c3b95e363e92eaa0bebc][=2019-04-18 00:21=]] Agregar hora en header date
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/5c8643b83930c6269569c76602608bd33f93008b][=2019-04-18 00:01=]] Corregir identación header #+INCLUDE:
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/bbc3bbc728f2a3eeb4fe2e0a012ee5d8d613e3ef][=2015-07-03 04:31=]] @ 00:05 hs - elimino #+OPTIONS: de todos los archivos excepto header.org
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/74165280ffad770d1f8b8acbfa7f22b95459b52a][=2014-04-22 11:35=]] @ 00:34 hs - Agrego timestamp:nil
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/5ad3755a3df07cdfbdc75d56cae06db2fee4b5f2][=2013-04-24 08:04=]] @ 01:50 hs - migro a org 8.0
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/00977674c60135495c431e80cae66c249d08aa42][=2012-12-16 09:50=]] @ 00:43 hs - Corrijo formato archivos.
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/652199f438b8e3b7f52720e2dc19208c9bcd7651][=2012-12-15 22:31=]] @ 04:00 hs - convert old blog in rST to org
