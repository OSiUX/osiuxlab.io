#+TITLE:       Flasheando un TPLink MR3220 con BATMAN Mesh en HackBo durante las Cuartas Jornadas Regionales Redes Libres
#+AUTHOR:      Osiris Alejandro Gomez
#+EMAIL:       osiux@osiux.com
#+LANGUAGE:    es
#+LINK_HOME:   index.html
#+INCLUDE:     header.org
#+DATE:        2012-06-30 22:19
#+HTML_HEAD:   <meta property="og:title" content="Flasheando un TPLink MR3220 con BATMAN Mesh en HackBo durante las Cuartas Jornadas Regionales Redes Libres" />
#+HTML_HEAD:   <meta property="og:type" content="article" />
#+HTML_HEAD:   <meta property="og:article:published_time" content="2012-06-30" />
#+HTML_HEAD:   <meta property="og:article:author" content="Osiris Alejandro Gomez" />
#+HTML_HEAD:   <meta property="og:url" content="https://osiux.com/2012-06-30-hackbo-jrrl4-flash-tplink-mr3220-batman-mesh.html" />
#+HTML_HEAD:   <meta property="og:site_name" content="OSiUX" />
#+HTML_HEAD:   <meta property="og:locale" content="es_AR" />


El 30 de Junio estuve en el [[http://hackbo.co/][HackBo]] dando una
charla/taller sobre como flashear un AccessPoint TPLink MR3320 usando
un firmware OpenWRT con soporte B.A.T.M.A.N. para armar una red mesh
usando el modelo [[http://qmp.cat/][QmP]] en base a las modificaciones
realizadas en las redes DeltaLibre y QuintanaLibre del programa
[[http://wiki.arraigodigital.org.ar/RedLibre/BatMesh?action=show&redirect=RedLibre/qMp-adv/Roadmap][ArraigoDigital]]

** video charla

#+BEGIN_HTML
  <video controls="controls" src="http://archive.org/download/jrrl4-hackbo-batmesh/jrrl4-hackbo-flash-tplink-mr3220-batman-mesh.ogv"></video>
#+END_HTML

- [[http://archive.org/download/jrrl4-hackbo-batmesh/jrrl4-hackbo-flash-tplink-mr3220-batman-mesh.ogv][Descargar Video Charla]]

** video pantalla

#+BEGIN_HTML
  <video controls="controls" src="http://pub.osiux.com/charlas/jrrl4-hackbo-flash-tplink-mr3220-batman-mesh-screen.ogv"></video>
#+END_HTML

- [[http://pub.osiux.com/charlas/jrrl4-hackbo-flash-tplink-mr3220-batman-mesh-screen.ogv][Descargar Video Pantalla]]

** imagenes OpenWRT release 31316 batmesh

*IMPORTANTE*: es recomendale usar nuevas versiones desde: [[http://chef.mesh.altermundi.net]]

- [[http://pub.osiux.com/batmesh/r31316/openwrt-ar71xx-generic-tl-mr3020-v1-squashfs-factory.bin>][918a3018a93e06624e2c09a43cac57a4  mr3020-factory]]
- [[http://pub.osiux.com/batmesh/r31316/openwrt-ar71xx-generic-tl-mr3020-v1-squashfs-sysupgrade.bin][dfe5c0b962e2d50df168082df1f496af  mr3020-sysupgrade]]
- [[http://pub.osiux.com/batmesh/r31316/openwrt-ar71xx-generic-tl-mr3220-v1-squashfs-factory.bin][bef9c2524e8b8ed30b4a50ef96ec14e5  mr3220-factory]]
- [[http://pub.osiux.com/batmesh/r31316/openwrt-ar71xx-generic-tl-mr3220-v1-squashfs-sysupgrade.bin][482be329482cb7764dfda632cf167e7f  mr3220-sysupgrade]]


** ChangeLog

  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/bf3a61526ad2a73cecb77a18995f1d63494e3664][=2022-11-13 20:39=]] agregar y actualizar tags OpenGraph
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/e46ec52748a7ecc60f09c3b95e363e92eaa0bebc][=2019-04-18 00:21=]] Agregar hora en header date
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/5c8643b83930c6269569c76602608bd33f93008b][=2019-04-18 00:01=]] Corregir identación header #+INCLUDE:
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/bbc3bbc728f2a3eeb4fe2e0a012ee5d8d613e3ef][=2015-07-03 04:31=]] @ 00:05 hs - elimino #+OPTIONS: de todos los archivos excepto header.org
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/74165280ffad770d1f8b8acbfa7f22b95459b52a][=2014-04-22 11:35=]] @ 00:34 hs - Agrego timestamp:nil
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/5ad3755a3df07cdfbdc75d56cae06db2fee4b5f2][=2013-04-24 08:04=]] @ 01:50 hs - migro a org 8.0
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/e4dc30e2a202e7ad44bddc761065e9aeb0a31bd3][=2013-03-21 02:27=]] @ 01:58 hs - agrego cambios según git.osiux.com
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/00977674c60135495c431e80cae66c249d08aa42][=2012-12-16 09:50=]] @ 00:43 hs - Corrijo formato archivos.
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/652199f438b8e3b7f52720e2dc19208c9bcd7651][=2012-12-15 22:31=]] @ 04:00 hs - convert old blog in rST to org
