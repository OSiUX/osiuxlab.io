#+TITLE:       3er. Evento de Software Libre y Economía Social
#+AUTHOR:      Osiris Alejandro Gomez
#+EMAIL:       osiux@osiux.com
#+LANGUAGE:    es
#+LINK_HOME:   index.html
#+INCLUDE:     header.org
#+DATE:        2012-06-13 22:19
#+HTML_HEAD:   <meta property="og:title" content="3er. Evento de Software Libre y Economía Social" />
#+HTML_HEAD:   <meta property="og:type" content="article" />
#+HTML_HEAD:   <meta property="og:article:published_time" content="2012-06-13" />
#+HTML_HEAD:   <meta property="og:article:author" content="Osiris Alejandro Gomez" />
#+HTML_HEAD:   <meta property="og:url" content="https://osiux.com/2012-06-13-SLyES-software-libre-y-economia-social-2012.html" />
#+HTML_HEAD:   <meta property="og:site_name" content="OSiUX" />
#+HTML_HEAD:   <meta property="og:locale" content="es_AR" />


Hoy desde las 16 horas los esperamos en el Centro Cultural de la
Cooperación (Sala Solidaridad, 2 subsuelo) para compartir experiencias e
intercambiar en el 3er. Evento de Software Libre y Economía Social.

Recordá que el evento es gratuito pero con inscripción previa desde:

- http://www.gcoop.coop/sles-2012-inscripcion

Consultá la agenda del evento:

- http://www.gcoop.coop/sles-2012

Desde las 12 horas estaremos programando en un hackaton cooperativo y al
final del Evento daremos nacimiento a la Federación Argentina de
Cooperativas de Trabajo de Tecnología, Innovación y Conocimiento
(FACTTIC).

- http://www.facttic.org.ar/

Sobre los invitados

Patricio Griffin
 Presidente del INAES desde 2007. Es Abogado, especializado en
 Derecho laboral y mercantil.

Carlos Heller
 Diputado Nacional por Nuevo Encuentro; Presidente del Banco
 Credicoop; militante cooperativista.

Pablo Fontdevila
 Gerente Ejecutivo del Programa Conectar Igualdad; Director del
 Departamento de Ingeniería en la Universidad Nacional de Tres de
 Febrero; ex Sub Director de Prestaciones del ANSES.

Pedro Pesatti
 Legislador Frente para La Victoria de Río Negro, autor de la Lay
 de uso de Software Libre en esa Provincia.

Patricia Giardini
 Directora General de Informática en Municipalidad de Rosario,
 municipalidad pionera en el uso de Soluciones Libres.

José Orbaiceta
 Presidente de la Federación de Cooperativas de Trabajo de la
 Argentina (FECOOTRA); vocal del Instituto Nacional de
 Asociativismo y Economía Social; vocal de Cooperar; militante
 cooperativista.

Juan Eugenio Ricci
 Actual Coordinador Federal y ex Presidente del INAES; militante de
 la Economía Social.

Pablo Recepter
 Gerente de Informática en Banco Credicoop (CIO); Ingeniero por la
 Universidad de La Plata; difusor del Software Libre.

Jorge Cabezas
 Coordinador del Proyecto Software Público Internacional en la
 Jefatura de Gabinete de Ministros de la República Argentina;
 organizador de la Conferencia Internacional de Software Libre
 (CISL) Argentina; militante político y social.

Christian Miño
 Presidente de la Confederación Nacional de Cooperativas de Trabajo
 (CNCT); miembro de FECOOTRA-UN de Florencia Varela.

Facundo Batista
 Ingeniero Electrónico y Master en Ingeniería de la Innovación de
 la Universidad de Bologna; Technical Leader en Canonical; miembro
 de la Python Software Foundation.

Alvaro Soliverez
 Desarrollador de KDE; miembro de Software Libre con Cristina
 Fernández de Kirchner.

Juan Manuel Schillaci
 Miembro Fundador de la Cooperativa de Trabajo de Desarrollo de
 Software Libre Devecoop; Analista de Sistemas.

- Invita: [[http://gcoop.coop][Cooperativa de Trabajo gcoop Ltda]]
- Auspician:
  [[http://www.fecootra.org.ar/][FECOOTRA]] y [[http://www.cnct.org.ar/][CNCT]]


** ChangeLog

  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/bf3a61526ad2a73cecb77a18995f1d63494e3664][=2022-11-13 20:39=]] agregar y actualizar tags OpenGraph
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/e46ec52748a7ecc60f09c3b95e363e92eaa0bebc][=2019-04-18 00:21=]] Agregar hora en header date
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/882b1cda084f7c18362c8414a78dd65c7b968a3f][=2019-04-09 03:01=]] Recuperar archivos en .md y convertirlos a .org
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/bbc3bbc728f2a3eeb4fe2e0a012ee5d8d613e3ef][=2015-07-03 04:31=]] @ 00:05 hs - elimino #+OPTIONS: de todos los archivos excepto header.org
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/74165280ffad770d1f8b8acbfa7f22b95459b52a][=2014-04-22 11:35=]] @ 00:34 hs - Agrego timestamp:nil
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/5ad3755a3df07cdfbdc75d56cae06db2fee4b5f2][=2013-04-24 08:04=]] @ 01:50 hs - migro a org 8.0
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/00977674c60135495c431e80cae66c249d08aa42][=2012-12-16 09:50=]] @ 00:43 hs - Corrijo formato archivos.
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/652199f438b8e3b7f52720e2dc19208c9bcd7651][=2012-12-15 22:31=]] @ 04:00 hs - convert old blog in rST to org
