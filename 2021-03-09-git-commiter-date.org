#+TITLE:       cambiar la fecha de un /commit/ usando =GIT_COMMITER_DATE=
#+AUTHOR:      Osiris Alejandro Gomez
#+EMAIL:       osiux@osiux.com
#+LANGUAGE:    es
#+LINK_HOME:   index.html
#+INCLUDE:     header.org
#+DATE:        2021-03-09 23:20
#+HTML_HEAD:   <meta property="og:title" content="cambiar la fecha de un /commit/ usando =GIT_COMMITER_DATE=" />
#+HTML_HEAD:   <meta property="og:type" content="article" />
#+HTML_HEAD:   <meta property="og:article:published_time" content="2021-03-09" />
#+HTML_HEAD:   <meta property="og:article:author" content="Osiris Alejandro Gomez" />
#+HTML_HEAD:   <meta property="og:url" content="https://osiux.com/2021-03-09-git-commiter-date.html" />
#+HTML_HEAD:   <meta property="og:site_name" content="OSiUX" />
#+HTML_HEAD:   <meta property="og:locale" content="es_AR" />



** la hora del /commit/

Muchas veces, estoy enajenado y/o enrroscado y considero que no estoy
para realizar el /commit/ porque le falta algo más y suele pasar que o
bien agrando la tarea o si de casualidad ya estaba listo para
/commitear/ y surgió algo por lo que tuve que alejar del teclado y se
pasó la hora, o el día del /commit/ y no esta bueno que la fecha de un
/commit/ no refleje la fecha y hora de cuando realmente se realizó (o
cuando se debería haber realizado).

** =commit= en pausa

Una estrategia que aprendí hace un par de años es la antes que termine
el día iniciar el /commit/ utilizando =git commit= pero dejándolo en
pausa, y a la mañana siguiente, café de por medio, un poco mas
tranquilo, redactar bien el comentario, con la ventaja de que al
terminar, la hora y fecha del /commit/ es el día anterior, que refleja
el esfuerzo tal y como fue.

** =GIT_COMMITER_DATE=

Se puede redefinir la fecha y hora de un /commit/ antes de realizar
utilizando la variable =GIT_COMMITER_DATE=, por ejemplo si quisiera
registrar que un /commit/ se hizo ayer =2021-03-09= a la misma hora de
hoy =2021-03-10 23:52= se puede basta ejecutar de la siguiente manera:

#+BEGIN_SRC sh :exports code

GIT_COMMITER_DATE="$(date -d 'now -1 days')" git commit --amend --date "$(date -d 'now -1 days')"

#+END_SRC

** la máquina del tiempo

Para generalizar, creé una función =gct= /git commit time/ y simplemente
especificando =25m=, =2h=, =1d=:

#+BEGIN_SRC sh :exports code

function gct()
{

  local D
  local N
  local T

  N="$(echo "$1" | grep -Eo "[0-9]+")"
  T="$(echo "$1" | grep -Eo "[a-z]+")"

  [[  -z   "$1" ]] && exit 1
  [[  -z   "$N" ]] && exit 1

  [[  -z   "$T" ]] && T='minutes'
  [[ "$T" = 'm' ]] && T='minutes'
  [[ "$T" = 'h' ]] && T='hours'
  [[ "$T" = 'd' ]] && T='days'

  D="$(date -d "now -$N $T")"
  GIT_COMMITTER_DATE="$D" git commit --date "$D"

}

#+END_SRC


** ChangeLog

  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/bf3a61526ad2a73cecb77a18995f1d63494e3664][=2022-11-13 20:39=]] agregar y actualizar tags OpenGraph
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/4d045bc4abecda5b8316a0e3b92faa315ad92206][=2021-03-09 23:54=]] agregar cambiar la fecha de un /commit/ usando =GIT_COMMITER_DATE=
