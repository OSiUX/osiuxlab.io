#+TITLE:       Huayra, el día que las vacas vuelen, llegó!
#+AUTHOR:      Osiris Alejandro Gomez
#+EMAIL:       osiux@osiux.com
#+LANGUAGE:    es
#+LINK_HOME:   index.html
#+INCLUDE:     header.org
#+DATE:        2013-03-18 05:39
#+HTML_HEAD:   <meta property="og:title" content="Huayra, el día que las vacas vuelen, llegó!" />
#+HTML_HEAD:   <meta property="og:type" content="article" />
#+HTML_HEAD:   <meta property="og:article:published_time" content="2013-03-18" />
#+HTML_HEAD:   <meta property="og:article:author" content="Osiris Alejandro Gomez" />
#+HTML_HEAD:   <meta property="og:url" content="https://osiux.com/2013-03-18-huayra-el-dia-que-las-vacas-vuelen.html" />
#+HTML_HEAD:   <meta property="og:site_name" content="OSiUX" />
#+HTML_HEAD:   <meta property="og:locale" content="es_AR" />
#+HTML_HEAD:   <meta property="og:image" content="https://osiux.com/img/huayra-vaca-volando.png" />


#+ATTR_HTML: :witdh 640 :height 500 :title Huayra
[[file:img/huayra-vaca-volando.png]]

Aproximadamente en 1999 comencé a usar GNU/Linux y era algo que muy
pocos conocían, costaba obtener un CD para instalarlo, descargarlo por
Internet era muy lento y lo que uno descargaba no siempre estaba listo
para instalar, en ocasiones había que compilar algunas cosas, lo mas
fácil era comprar alguna revista española que trajera el CD, la
alternativa era conocer a alguien que tenga un CD de la distribución
que te interesaba probar.

En esa época era impensado que Linux fuera popular, algunos LUGs, es
decir algunos grupos de usuarios se juntaban y hacían "eventos" para
compartir lo que sabían con el resto de la comunidad, desde entonces
asistí a cuanto evento y reunión hubiera para conocer más al respecto
y sumado a las horas de practica fui aprendiendo.

Si bien, desde el principio, los difusores de las virtudes y ventajas
del software libre soñaban con que todos migraran de sistema
operativo, era impensado llegar a millones de personas que usen
diariamente alguna versión de GNU/Linux. En estos últimos años ya no
es raro decir GNU/Linux o simplemente Linux, miles de nuevos usuarios
tienen/usan/conocen alguna distro como Debian, Ubuntu, Fedora, Suse,
Mandriva...

Pero hace relativamente poco, el Estado Nacional a través
del *Programa Conectar Igualdad* [fn:PCI], no sólo se propuso un
cambio revolucionario en la educación de nuestros jóvenes, "entregar
una notebook por alumno" con todo lo que esto implica, ya de por si,
entregar 3 millones de equipos a lo largo y ancho de nuestro país es
todo un desafío, cada escuela, cada docente, cada joven y cada familia
debe tener su historia al respecto.

Lo impensado es que estas notebooks tuvieran un sistema operativo
basado en Debian, con desarrollo local, inmerso dentro del plan
educativo, con soporte local, hecho por militantes del software libre,
personas que hace muchos mas años que yo vienen usando y difundiendo
el software libre.

Si en 1999 me hubieran dicho que todo esto era posible algún día, yo
hubiera dicho que seria el *día que las vacas vuelen*, y *ese día
llego*! *Huayra 1.0 esta disponible* [fn:huayra] no solo para las
notebooks de las escuelas del país, sino para cualquiera que tenga
ganas de usarlo e incluso mejorarlo, yo ya lo estoy usando e invito a
todos a probarlo!

[[http://linuxtracker.org/download.php?id=ef0968d17295e1c03f4e2868b363c6f9c7691400&f=Huayra+VR+GNU%2FLinux+1.0.torrent&key=6c2d037a][Descargar .torrent]]

[fn:PCI] http://www.conectarigualdad.gob.ar/
[fn:huayra] [[http://huayra.conectarigualdad.gob.ar/]]

** ChangeLog

  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/bf3a61526ad2a73cecb77a18995f1d63494e3664][=2022-11-13 20:39=]] agregar y actualizar tags OpenGraph
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/e46ec52748a7ecc60f09c3b95e363e92eaa0bebc][=2019-04-18 00:21=]] Agregar hora en header date
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/5c8643b83930c6269569c76602608bd33f93008b][=2019-04-18 00:01=]] Corregir identación header #+INCLUDE:
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/bbc3bbc728f2a3eeb4fe2e0a012ee5d8d613e3ef][=2015-07-03 04:31=]] @ 00:05 hs - elimino #+OPTIONS: de todos los archivos excepto header.org
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/a2abb3b333c91e0d3c15ea9e93a21589bd4d86f7][=2015-07-03 03:59=]] @ 01:00 hs - reemplazo :alt por :title y cambios menores
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/74165280ffad770d1f8b8acbfa7f22b95459b52a][=2014-04-22 11:35=]] @ 00:34 hs - Agrego timestamp:nil
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/cfd0eadabe370dc0df4729fd3de6b3637a022338][=2013-05-06 10:14=]] @ 00:10 hs - varias correcciones menores
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/5ad3755a3df07cdfbdc75d56cae06db2fee4b5f2][=2013-04-24 08:04=]] @ 01:50 hs - migro a org 8.0
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/8e366ba5e87f8fe3a9612ca9a3fdd3eb5b31577a][=2013-03-19 05:46=]] @ 00:50 hs - Agrego Huayra 1.0
