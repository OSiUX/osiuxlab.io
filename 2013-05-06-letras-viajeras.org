#+TITLE:       gcoop libera "Letras Viajeras"
#+AUTHOR:      Osiris Alejandro Gomez
#+EMAIL:       osiux@osiux.com
#+LANGUAGE:    es
#+LINK_HOME:   index.html
#+INCLUDE:     header.org
#+DATE:        2013-05-06 16:21
#+HTML_HEAD:   <meta property="og:title" content="gcoop libera "Letras Viajeras"" />
#+HTML_HEAD:   <meta property="og:type" content="article" />
#+HTML_HEAD:   <meta property="og:article:published_time" content="2013-05-06" />
#+HTML_HEAD:   <meta property="og:article:author" content="Osiris Alejandro Gomez" />
#+HTML_HEAD:   <meta property="og:url" content="https://osiux.com/2013-05-06-letras-viajeras.html" />
#+HTML_HEAD:   <meta property="og:site_name" content="OSiUX" />
#+HTML_HEAD:   <meta property="og:locale" content="es_AR" />
#+HTML_HEAD:   <meta property="og:image" content="https://osiux.com/img/letras-viajeras.jpg" />


#+ATTR_HTML: :width 640 :height 388 :title Letras Viajeras
[[file:img/letras-viajeras.jpg]]

**Letras Viajeras** es una iniciativa de la
**Dirección de Bibliotecas y Promoción de la Lectura del Gobierno de la Provincia de Buenos Aires**,
que permitirá a los pasajeros de larga y media distancia tener
acceso gratuito a libros digitales, con sus dispositivos móviles a
través de una conexión Wi-fi que permitirá descargar libros y
contenidos literarios seleccionados por la Dirección.

El material de descarga (libros, cuentos y revistas) son materiales
del dominio público y materiales liberados.

Letras Viajeras fue desarrollado íntegramente con Software Libre por
la /Cooperativa de trabajo gcoop/ [fn:gcoop]. La solución consiste en
un Router Inalámbrico al que se le instala =OpenWRT= [fn:openwrt], un
Fimware libre basado en /Linux/.

Por otro lado se desarrolló una aplicación con =Flask= [fn:flask] y
=Boostrap= [fn:boostrap] para generar el Portal web que corre en el
Router y que permite ver y acceder a todos los materiales disponibles
para descarga.

Además de la liberación del código fuente [fn:github] se realizó un
tutorial [fn:tutorial] con el fin de que el dispositivo (sin los
materiales) pueda ser realizado por cualquier persona o institución.

[fn:boostrap] http://twitter.github.io/bootstrap
[fn:flask] http://flask.pocoo.org
[fn:openwrt] http://openwrt.org
[fn:gcoop] http://gcoop.coop
[fn:github] https://github.com/gcoop-libre/letras_viajeras
[fn:tutorial] https://github.com/gcoop-libre/letras_viajeras/blob/master/GUIA_INSTALACION.md

** ChangeLog

  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/bf3a61526ad2a73cecb77a18995f1d63494e3664][=2022-11-13 20:39=]] agregar y actualizar tags OpenGraph
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/e46ec52748a7ecc60f09c3b95e363e92eaa0bebc][=2019-04-18 00:21=]] Agregar hora en header date
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/5c8643b83930c6269569c76602608bd33f93008b][=2019-04-18 00:01=]] Corregir identación header #+INCLUDE:
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/bbc3bbc728f2a3eeb4fe2e0a012ee5d8d613e3ef][=2015-07-03 04:31=]] @ 00:05 hs - elimino #+OPTIONS: de todos los archivos excepto header.org
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/a2abb3b333c91e0d3c15ea9e93a21589bd4d86f7][=2015-07-03 03:59=]] @ 01:00 hs - reemplazo :alt por :title y cambios menores
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/74165280ffad770d1f8b8acbfa7f22b95459b52a][=2014-04-22 11:35=]] @ 00:34 hs - Agrego timestamp:nil
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/9f75ffdfa6b6b7314651cb83632f3562d2b9297d][=2013-05-06 16:32=]] @ 00:45 hs - Agrego letras viajeras
